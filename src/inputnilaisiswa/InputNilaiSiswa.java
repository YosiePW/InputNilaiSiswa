package inputnilaisiswa;

import java.util.Scanner;

public class InputNilaiSiswa {

    public static void main(String[] args) {

        Scanner masukan = new Scanner(System.in);
        int ulangi = 0;
        do {
            System.out.println("Orang yang berhak melihat rapot siswa: ");
            System.out.println("1. Kepala Sekolah");
            System.out.println("2. Guru");
            System.out.println("3. Orang tua");
            System.out.println("4. Siswa");
            System.out.print("Masukkan nomor pilihan anda: ");
            int pilih = masukan.nextInt();

            if (pilih == 1) {
                System.out.println("Selamat Datang Di Data Nilai Siawa");
                System.out.println("Pilihan Untuk Anda");
                System.out.println("1. Lihat Data Nilai Siswa");
                System.out.println("2. Masukkan Nilai Pada Data Nilai Siswa");
                System.out.print("Masukkan Nomor Pilihan Anda : ");
                int pilih2 = masukan.nextInt();
                if (pilih2 == 1) {
                    System.out.println("Data Nilai Siswa");
                    String nama1 = "Kim Taehyung";
                    System.out.println("A. Nama : " + nama1);
                    double matematika = 75;
                    double ipa = 90;
                    double bahasaIndo = 85;
                    double big = 75;
                    System.out.println("NILAI :");
                    System.out.println("1. Matematika : " + matematika);
                    System.out.println("2. IPA : " + ipa);
                    System.out.println("3. Bahasa Indonesia : " + bahasaIndo);
                    System.out.println("4. Bahasa Inggris : " + big);
                    double ratarata;
                    ratarata = matematika + ipa + bahasaIndo + big / 4;
                    System.out.println("Rata-Rata = " + ratarata);
                    System.out.println("");
                    String nama2 = "Haruno Sakura";
                    System.out.println("B. Nama : " + nama2);
                    double matematika2 = 80;
                    double ipa2 = 91;
                    double bahasaIndo2 = 75;
                    double big2 = 70;
                    System.out.println("NILAI :");
                    System.out.println("1. Matematika : " + matematika2);
                    System.out.println("2. IPA : " + ipa2);
                    System.out.println("3. Bahasa Indonesia : " + bahasaIndo2);
                    System.out.println("4. Bahasa Inggris : " + big2);
                    double ratarata2;
                    ratarata2 = matematika2 + ipa2 + bahasaIndo2 + big2 / 4;
                    System.out.println("Rata-Rata = " + ratarata2);
                }

                if (pilih2 == 2) {
                    System.out.print("Masukkan Nama Siswa : ");
                    String nama = masukan.nextLine();
                    System.out.println("Masukkan Nilai : ");
                    System.out.print("1. Matematika : ");
                    int Mtk = masukan.nextInt();
                    System.out.print("2.IPA : ");
                    int Ipa = masukan.nextInt();
                    System.out.print("3.Bahasa Indonesia : ");
                    int Indo = masukan.nextInt();
                    System.out.print("Bahasa Inggris : ");
                    int big = masukan.nextInt();
                    double ratarata;
                    ratarata = Mtk + Ipa + Indo + big / 4;
                    System.out.println("Rata-Rata = " + ratarata);
                }
            }
            if (pilih == 2) {
                System.out.println("Nilai raport siswa : ");
                System.out.println("Pilihan Untuk anda : ");
                System.out.println("1. Lihat Data Nilai Siswa");
                System.out.println("2. Masukkan Nilai Pada Data Nilai Siswa");
                System.out.print("Masukkan Nomor Pilihan Anda : ");
                int pilih3 = masukan.nextInt();
                if (pilih3 == 1) {
                    System.out.println("Data Nilai Siswa");
                    String nama1 = "Kim Taehyung";
                    System.out.println("A. Nama : " + nama1);
                    double matematika = 75;
                    double ipa = 90;
                    double bahasaIndo = 85;
                    double big = 75;
                    System.out.println("NILAI :");
                    System.out.println("1. Matematika : " + matematika);
                    System.out.println("2. IPA : " + ipa);
                    System.out.println("3. Bahasa Indonesia : " + bahasaIndo);
                    System.out.println("4. Bahasa Inggris : " + big);
                    double ratarata;
                    ratarata = matematika + ipa + bahasaIndo + big/ 4;
                    System.out.println("Rata-Rata = " + ratarata);
                    System.out.println("");
                    String nama2 = "Haruno Sakura";
                    System.out.println("B. Nama : " + nama2);
                    double matematika2 = 80;
                    double ipa2 = 91;
                    double bahasaIndo2 = 75;
                    double big2 = 70; 
                    System.out.println("NILAI :");
                    System.out.println("1. Matematika : " + matematika2);
                    System.out.println("2. IPA : " + ipa2);
                    System.out.println("3. Bahasa Indonesia : " + bahasaIndo2);
                    System.out.println("4. Bahasa Inggris : " + big2);
                    double ratarata2;
                    ratarata2 = matematika2 + ipa2 + bahasaIndo2 + big2 / 4;
                    System.out.println("Rata-Rata = " + ratarata2);
                }
                if (pilih3 == 2) {
                    System.out.println("Ubah Nilai Raport Siswa");
                    System.out.print("Masukkan Nama Siswa : ");
                    String nama = masukan.nextLine();
                    System.out.println("Masukkan Nilai : ");
                    System.out.print("1. Matematika : ");
                    int Mtk = masukan.nextInt();
                    System.out.print("2.IPA : ");
                    int Ipa = masukan.nextInt();
                    System.out.print("3.Bahasa Indonesia : ");
                    int Indo = masukan.nextInt();
                    System.out.print("4.Bahasa Inggris : ");
                    int big = masukan.nextInt();
                    double ratarata;
                    ratarata = Mtk + Ipa + Indo + big / 4;
                    System.out.println("Rata-Rata = " + ratarata);
                }
            }

            if (pilih == 3) {
                System.out.println("Nilai raport anak anda : ");
                String nama2 = "Haruno Sakura";
                System.out.println("B. Nama : " + nama2);
                double matematika2 = 80;
                double ipa2 = 91;
                double bahasaIndo2 = 75;
                double big2 = 70;
                System.out.println("NILAI :");
                System.out.println("1. Matematika : " + matematika2);
                System.out.println("2. IPA : " + ipa2);
                System.out.println("3. Bahasa Indonesia : " + bahasaIndo2);
                System.out.println("4. Bahasa Inggris : " + big2);
                double ratarata2;
                ratarata2 = matematika2 + ipa2 + bahasaIndo2 + big2/ 4;
                System.out.println("Rata-Rata = " + ratarata2);
            }
            if (pilih == 4) {
                System.out.println("Nilai raport anda : ");
                String nama2 = "Haruno Sakura";
                System.out.println("B. Nama : " + nama2);
                double matematika2 = 80;
                double ipa2 = 91;
                double bahasaIndo2 = 75;
                double big2 = 70;
                System.out.println("NILAI :");
                System.out.println("1. Matematika : " + matematika2);
                System.out.println("2. IPA : " + ipa2);
                System.out.println("3. Bahasa Indonesia : " + bahasaIndo2);
                System.out.println("4. Bahasa Inggris : " + big2);
                double ratarata2;
                ratarata2 = matematika2 + ipa2 + bahasaIndo2 + big2 / 4;
                System.out.println("Rata-Rata = " + ratarata2);
            }
            System.out.println("Apakah anda ingin mengulangi? Yes for 1 dan 0 for No");
            System.out.print("Masukkan pilihan : ");
            ulangi = masukan.nextInt();
        } while (ulangi == 1);
    }
}
